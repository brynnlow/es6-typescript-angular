# Angular (partie 2)

Quels composants sont mis à notre disposition et comment s'imbriquent-ils ?

## Les composants principaux

### Les modules
A la racine, nous avons les modules. Les modules représentent une partie de notre application. Imaginons un blog avec des utilisateurs enregistrés, nous pourrions avoir un module pour les pages contenant les articles et un autre module pour les pages de profil de l'utilisateur ("mon compte", "mes préférences", 
"mes commentaires", ...).
 
Concrètement un module va être représenté par un fichier qui va consigner la liste des `components` qu'il contient.
 
### Les components
Ils sont le coeur du système, ce sont eux qui vont gérer ce qu'on va afficher et les données qui seront affichées. Ils sont en général composés d'un fichier .ts qui servira de *controller* d'un fichier .html qui sera la *vue* et d'un fichier .css pour le style. Donc en gros le fichier html affichera les données fournies par le fichier .ts et sera mis en page par le fichier .css
 
### Le router
Celui ci va nous permettre de créer des `routes` (ce qui sera dans l'url) et de les associer à des `compnents`.

### Les directives
Grosso modo ce sont des traitements que nous pouvons assigner certains affichages.

## Comment les appréhender ?

Pour un premier projet simple, considérez le premier component [AppComponent](mon-premier-projet/src/app/app.component.ts) comme le container principal de notre application. Les "pages" seront gérées par d'autres components que nous créerons nous-mêmes.

Tout ce que nous afficherons fera partie d'un `component`. Voyons en détail ces fameux `components`.

## Les components
Les components, nous l'avons dit sont donc un fichier ts qui va fournir des données à un fichier html qu'il va afficher.

### Le décorator

Un component est défini par son *decorator* `@Component`. On le place juste au dessus de la déclaration de la classe. 

Il nous permet de définir plusieurs propriétés du component. On le voit dans [AppComponent](mon-premier-projet/src/app/app.component.ts) il définit `selector`, `templateUrl` et `styleUrls`.

#### selector
C'est un élément primordial. Il permet à notre component d'être intégré à un autre component depuis son fichier html. Si le `selector` à la valeur "app-root" un autre component pourra l'intégrer à sa vue en écrivant la valeur de son `selector` dans une balise `<app-root></app-root>`. 

**On peut donc imbriquer nos components les uns dans les autres**

#### templateUrl
C'est le chemin vers le fichier html qui sera affiché par le component.

#### styleUrls
C'est un array contenant les fichiers css qui seront associés à la vue *.html*

### Notre premier component
Pour aider à mieux comprendre mettons ce que nous avons vu en pratique et créeons notre premier component.

Afin de se faciliter la vie avec l'affichage, je vous propose d'installer [Bootstrap](https://getbootstrap.com/) dans notre projet en tapant la commande suivante `npm install bootstrap --save` et d'aller intégrer la librairie css dans le fichier [angular.json](mon-premier-projet/angular.json) dans le noeud "architect.build.options".

Tapez maintenant la commande `ng generate component my-nav` depuis le dossier racine de notre projet angular pour créer notre premier component qui contiendra notre menu.

#### Que s'est il passé ?
- angular-cli à créé un dossier my-nav. Dedans il a généré un fichier .ts, .html, un .css et un .spec (pour les tests, nous l'ignorons pour le moment).
- Il a aussi été enregistrer notre component dans le module app afin de le rendre disponible depuis l'intérieur de notre module app (voir le *decorator* `@Module` dans la propriété `declarations`. Sachez que pour le rendre disponible hors module il faut aussi le renseigner dans la propriété `exports` de ce meme *decorator*.


#### Créeons une ébauche de menu.
Rendons nous dans le fichier [my-nav.component.html](mon-premier-projet/src/app/my-nav/my-nav.component.html) et collons ce bout de code html :
```
<nav>
     <ul class="nav justify-content-end">
       <li class="nav-item">
         <a class="nav-link active" href="#">Accueil</a>
       </li>
       <li class="nav-item">
         <a class="nav-link" href="#">Réalisations</a>
       </li>
       <li class="nav-item">
         <a class="nav-link" href="#">Contact</a>
       </li>
     </ul>
   </nav>
```

Il nous reste à intégrer notre component à la vue générale. En effet ce component doit etre visible sur toutes les pages de notre site.

Nous allons aussi retirer le contenu de la page par defaut d'angular en vidant le fichier [app.component.html](mon-premier-projet/src/app/app.component.html)

Pour ce faire, utilisons son selector remplacons le contenu de [app.component.html](mon-premier-projet/src/app/app.component.html) par cette balise : `<app-my-nav></app-my-nav>` .

Pour le principe, modifions un peu son apparence en mettant un background un peu bateau à notre menu et deux trois autres choses. Allons dans [my-nav.component.css](mon-premier-projet/src/app/my-nav/my-nav.component.css) et ajoutons cette règle css : 
```
nav { background: #333; padding: 1rem 0;}
a {color: #ddd;}
a:hover {color: #fff;}
``` 
Magie, notre page se rafraichit toute seule (si vous n'aviez pas oublié de lancé le server avec la commande `ng serve
` préalablement). La css que nous avons ajouté est appliquée et ne sera valable que pour le contenu du fichier [my-nav.component.html](mon-premier-projet/src/app/my-nav/my-nav.component.html).
