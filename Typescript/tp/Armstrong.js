"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Armstrong {
    constructor(firstName, id) {
        // readonly, une propriété qui ne peut jamais changer (comme une const)
        // on peut lui donner une valeur à l'instanciation (via le constructeur par exemple) mais ici ce n'est pas nécéssaire
        this.lastName = "Armstrong";
        this.selected = false;
        this.firstName = firstName;
        this.id = id;
    }
}
exports.Armstrong = Armstrong;
