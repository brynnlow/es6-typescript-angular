import {Armstrong} from './Armstrong'; // On ne note plus l'extension de fichier

const armstrongsJson = '[{"id": 1, "lastName": "Armstrong", "firstName": "Louis"},{"id": 2, "lastName": "Armstrong", "firstName": "Neil"},{"id": 3, "lastName": "Armstrong", "firstName": "Lance"}]';

let armtrongs: Armstrong[] = [];


for(let armstrongJson of JSON.parse(armstrongsJson)){
    armtrongs.push(armstrongJson); // Il transforme tout seul le json dans le bon objet
}

createForm();
listenActive();
summary();

function createForm() {
    let parent = document.getElementById("armstrongs");
    if (parent !== null){
        armtrongs.forEach(armstrong => {
            // @ts-ignore
            parent.innerHTML += '<li class="list-group-item" data-id="'+armstrong.id+'">' + armstrong.firstName + ' ' + armstrong.lastName + '</li>';
        })
    }

}

function listenActive() {
    document.querySelectorAll('.list-group-item').forEach(element => element.addEventListener('click', listClicked));
}


function listClicked(event: Event) {
    console.log("clicked");
    const target = event.target as HTMLLIElement;

    if (target.classList.contains("active")){
        target.classList.remove("active");
        // @ts-ignore
        armtrongs
            .find(armstrong => {
                // @ts-ignore
                return armstrong.id == parseInt(target.getAttribute("data-id"));
            })
            .selected = false;

    }else{
        target.classList.add("active");
        // @ts-ignore
        armtrongs
            .find(armstrong => {
                // @ts-ignore
                return armstrong.id == parseInt(target.getAttribute("data-id"));
            })
            .selected = true;

    }
    summary();
}


function summary() {
    const selected = armtrongs.filter(armtrong => armtrong.selected);

    console.log(selected);

    let message = "Aucun Armstrong de sélectionné";
    let classAlert = "danger";

    if (selected.length) {
        message = selected.map(armtrong => armtrong.firstName).join(', ') + ' sélectionné(s)';
        classAlert = 'info';
    }

    // @ts-ignore
    document.getElementById("summary").innerHTML = '<div class="alert alert-'+classAlert+'" role="alert"> \n' +
         message + '  \n' +
        '</div>';

}
