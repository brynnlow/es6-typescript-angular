class Person {
    firstName: string;
    lastName: string;

    talk(message: string){
        const messageWithName = this.firstName + ' ' + this.lastName + ' : ' + message;
        console.log(messageWithName);
    }
}

const joueur1 = new Person();
const joueur2 = new Person();

joueur1.firstName = "Marty";
joueur1.lastName = "Mac Fly";

joueur2.firstName = "Emmet";
joueur2.lastName = "Brown";

joueur1.talk("Salut Doc");
joueur2.talk("Nom de Zeus !");

//Si on veut assigner une valeur d'un mauvais type, nous aurons une erreur dans notre IDE.
// joueur2.talk(1);



