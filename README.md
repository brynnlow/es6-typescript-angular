# Bienvenue dans le cours de script client dédié à l'ES 6 et Angular

Ce cours se veut une introduction à l'ES6 et Angular. Il n'y a pas la place dans le programme pour apprendre ce framework en profondeur, mais nous allons nous débrouiller pour en apprendre suffisemment que pour nous permettre de lancer un projet avec.

## Sommaire

1. [**NPM**: Gestionnaire de dépendances indispensable](npm/)
2. **[ES6](ES6)** et **[Typescript](Typescript)**
3. Angular
    0. [Introduction](angular)
    1. [Installation et Structure des fichiers](angular/introduction)
    2. [Structure Framework](angular/structure_du_framwork)
    3. Data Binding
    4. Directives
    5. Services
    6. Component detail
    7. Naviguation
    8. RxJS

  
