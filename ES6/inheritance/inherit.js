class Person {

    firstName;
    lastName;

    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    talk(message){
        const messageWithName = this.firstName + ' ' + this.lastName + ' : ' + message;
        console.log(messageWithName);
    }
}

class Wizard extends Person {
    talkToGods(message){
        this.talk(btoa(message)); // btoa() encode une chaine en base64
    }
}

class Warrior extends Person {
    hitSomebody(person) {
        this.talk("Prends ça !");
        person.talk("aieuu");
    }
}

const joueur1 = new Warrior("Marty", "Mac Fly");
const joueur2 = new Wizard("Emmet", "Brown");


joueur1.talk("Salut Doc");
joueur2.talk("Nom de Zeus !");

joueur1.hitSomebody(joueur2);
joueur2.talkToGods("Nom de Zeus !!");


