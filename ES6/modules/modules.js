import {Warrior} from './warrior.class.js'
import {Wizard} from './wizard.class.js'


const joueur1 = new Warrior("Marty", "Mac Fly");
const joueur2 = new Wizard("Emmet", "Brown");


joueur1.talk("Salut Doc");
joueur2.talk("Nom de Zeus !");

joueur1.hitSomebody(joueur2);
joueur2.talkToGods("Nom de Zeus !!");


