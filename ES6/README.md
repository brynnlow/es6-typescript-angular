# ES6 pour ECMAScript6.

Comme d'habitude pour une bonne définition je vous renvoie vers la [fiche wikipedia](https://fr.wikipedia.org/wiki/ECMAScript). En gros sachez que ECMAScript est un ensemble de normes et que la version 6 est la version la plus actuelle supportée par une grosse partie des navigateurs.

# Ce qui change avec ES6

Pour cette fois je vous renvoie vers [W3C](https://www.w3schools.com/js/js_es6.asp) qui fait un petit résumé des changements les plus importants.

## Déclaration des variables

Les variables se déclarent désormais avec le mot clé `let`. Le mot clé `const` est apparu pour nommer des espaces mémoire pour lesquels la valeur assignée ne peut pas changer (ou ne change jamais).

## Les Classes

Depuis ES6, nous pouvons décrire nos futurs objets au moyen de classes. Un classe est donc du code javascript qui va décrire les particularités et le fonctionnement des objets qui en seront l'**instance**.
On crée une classe au moyen du mot clé `class`. 
Regardons ensemble [ce fichier](classes) (n'oubliez pas d'ouvrir la console).

- Une classe (Person) a été utilisée pour décrire ce qu'était un personnage dans notre code. 
- Nous avons créé deux **instances** de `Person` au moyen du mot clé `new`. Ces instances sont indépendantes l'une de l'autre mais partagent les deux **propriétés** `firstName` et `lastName` et la **methode** `talk(message)` qui en soi n'est qu'une fonction (au contraire des propriétés qui elles sont des variables).
- Depuis l'intérieur de la classe, nous accédons aux propriétés et aux methodes de l'objet avec le mot clé `this`
- **Un objet est donc l'instance d'une classe** On peut faire autant d'instances qu'on veut d'une meme classe.
- Prenez l'habitude de nommer vos classes avec un majuscule en première lettre. C'est une convention respectée par (presque) tout le monde.

### Les propriétés

Nous l'avons vu, les propriétés sont des variables appartenant à la classe. On peut faire une analogie avec les propriétés d'un objet de la vie réelle. Par exemple une machine à laver à une proprété `poids` qui pourrait avoir la valeur `60kg` ou `couleur` qui pourrait avoir la valeur `#FFF` (c'est du blanc évidemment). Ces propriétés ne sont rien d'autres que des valeurs à qui on a donné un nom. Elles ne font rien mais peuvent impacter le comportement de l'objet.

### Les methodes

Les méthodes, au contraire des propriétés font des choses. Concrètement ce sont des `function`. Elles executent des instructions.  

### Le constructeur

Le constructeur est une méthode spéciale qui s'execute automatiquement à l'instanciation (quand on fait `new`). Dans [cet exemple](constructor) il nous permet d'assigner des valeurs aux propriétés du futur objet. Le constructeur est une méthode qui doit obligatoirement s'appeler `constructor`.

### L'héritage
En Programation orientée objet, une classe peut *hériter* d'une autre grace au mot clé `extends`. Une classe qui hérite d'une autre hérite de toutes ces propriétés et méthodes. 

Pour faire une analgoie avec la vie réelle nous pourrions imaginer une classe `Mamifère` qui aurait certaines méthodes comme `allaiter()`. Nous pourrions aussi imaginer plusieurs classes différentes qui hériteraient de `Mamifère` comme `Chien` ou `Chat`. Un chien n'est pas un chat. Un chat n'est pas un chien. Par contre ils ont tous les deux hérité de la methode `allaiter()`. On peut dire qu'ils sont tous deux `Mamifère` mais que n'importe quel `Mamifère` n'est pas un `Chien` ou un `Chat`

Dans l'[exemple](inheritance), mes classes `Wizard` et `Warrior` héritent toutes deux des propriétés de `Person` mais ont des methodes différentes qui leur sont propres. 

## Les modules
[source](https://medium.com/backticks-tildes/introduction-to-es6-modules-49956f580da)

Les modules doivent etre considérés comme de petites unités de code réutilisable. Concrètement nous allons éclater notre code en plusieurs fichiers, et nous allons "importer" nos classes, nos functions ou nos variables dans d'autres fichiers.

Dans l'[exemple](modules) nous avons fait un fichier pour chaque classe. Nous avons ensuite importé les classes dans les fichiers qui en ont besoin. (`Person` dans `Wizard` et `Warrior`, `Wizard` et `Warrior` dans le fichier principal).

Pour que ça marche nous avons du ajouter l'attribut `type="module"` dans la balise `<script>` qui import notre premier fichier.

Pour importer nos classes nous utilisons l'instruction `import {NomDeLaClasse, EventuellementUneDeuxieme} from 'pchemin/vers_le_fichier.js'`. Ces classes ont été rendues importables grace au mot clé `export` présent devant.  


## Les boucles
[source](https://hacks.mozilla.org/2015/04/es6-in-depth-iterators-and-the-for-of-loop/)
Les boucles ont été un peu améliorées avec l'arrivée de ES6.  Nous disposons maintenant d'un outil pour faire des boucles plus facilement non seulement sur les tableaux, les objets litéraux, mais aussi les strings (on va passer d'un charactère à un autre).
C'est outil c'est le for-of.




## Les arrow functions

Les arrow functions sont une nouvelle manière d'écrire les functions. Globalement c'est du *sucre syntaxique* c'est à dire que nous ne sommes pas obligés de l'utiliser mais ça permet d'avoir une écriture plus directe.

[Pour l'exemple](arrow_functions) nous allons utiliser une nouvelle méthode disponibles sur les arrays : `find()`. Cette méthode nous renvoie le premier élément d'un tableau correspondant à un critère que nous pouvons définir.
Il faut voir cette méthode comme une méthode appelée sur chaque élément du tableau en boucle. L'argument de la function (`element` dans l'exemple) est l'élément courant de "la boucle". Dès que la callback function return `true` la "boucle" s'arrete et la méthode find retourne l'élément courant.

[Retour](..)
