class Person {

    firstName;
    lastName;
    isTheBoss;


    constructor(firstName, lastName, isTheBoss) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isTheBoss = isTheBoss;
    }

    talk(message){
        const messageWithName = this.firstName + ' ' + this.lastName + ' : ' + message;
        console.log(messageWithName);
    }
}

const joueur1 = new Person("Marty", "Mac Fly", false);
const joueur2 = new Person("Emmet", "Brown", true);

const joueurs = [joueur1, joueur2];

joueur1.talk("Salut Doc");
joueur2.talk("Nom de Zeus !");

joueurs.find(element => {
    return element.isTheBoss;
    })
    .talk("I'm the boss");


// Marche aussi. Pas de return nécéssaire parce que sur une ligne. Le return est implicite
joueurs.find(element => element.isTheBoss).talk("I'm still the boss");


// A l'ancienne
joueurs.find(function(element){
  return  element.isTheBoss;
} ).talk("I'm definitely the boss");



