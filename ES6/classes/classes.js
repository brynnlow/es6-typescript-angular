class Person {
    firstName;
    lastName;

    talk(message){
        const messageWithName = this.firstName + ' ' + this.lastName + ' : ' + message;
        console.log(messageWithName);
    }
}

const joueur1 = new Person();
const joueur2 = new Person();

joueur1.firstName = "Marty";
joueur1.lastName = "Mac Fly";

joueur2.firstName = "Emmet";
joueur2.lastName = "Brown";

joueur1.talk("Salut Doc");
joueur2.talk("Nom de Zeus !");


