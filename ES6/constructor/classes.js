class Person {

    firstName;
    lastName;

    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    talk(message){
        const messageWithName = this.firstName + ' ' + this.lastName + ' : ' + message;
        console.log(messageWithName);
    }
}

const joueur1 = new Person("Marty", "Mac Fly");
const joueur2 = new Person("Emmet", "Brown");


joueur1.talk("Salut Doc");
joueur2.talk("Nom de Zeus !");


