const myArray = ["a","b","c"];

//For of loop
for (let element of myArray){
    console.log(element);
}

const myString = "def";

//For of loop
for (let element of myString){
    console.log(element);
}
